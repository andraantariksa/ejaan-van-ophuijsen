# Ejaan Van Ophuijsen (Ejaan Lama)

## Sejarah singkat ejaan bahasa Indonesia

Sejak zaman dahulu, bahasa Melayu sudah memiliki ejaan yang berlaku. Ejaan yang berlaku pada zaman Belanda bernama Ejaan van Ophujsen. Ejaan ini digunakan untuk menuliskan kata-kata bahasa Melayu menurut model yang dimengerti oleh orang Belanda, yaitu menggunakan huruf Latin dan bunyi yang mirip dengan tuturan Belanda. Ejaan ini berlaku sejak tahun 1901.

Setelah Indonesia merdeka, disusunlah ejaan baru yang merupakan perbaikan Ejaan van Ophuijsen. Ejaan itu diberi nama Ejaan Republik atau Ejaan Republik atau Ejaan Soewandi. Pemilihan nama Ejaan Republik ini dikaitkan dengan peristiwa sejarah kemerdekaan negara kita dan pemilihan nama Ejaan Soewandi dikaitkan dengan nama Menteri Pendidikan dan Kebudayaan waktu itu, yaitu Soewandi. Ejaan Soewandi mulai berlaku sejak tahun 1947.

Setelah lebih dari dua dasawarsa Ejaan Soewandi berlaku, diberlakukan Ejaan Bahasa Indonesia yang Disempurnakan. Ejaan itu diresmikan pemberlakuannya oleh Presiden Soeharto berdasarkan Surat Keputusan Presiden Nomor 57 Tahun 1972.

Untuk sekarang, Indonesia sudah menggunakan Ejaan Bahasa Indonesia yang berlaku sejak tahun 2015 berdasarkan Peraturan Menteri Pendidikan dan Kebudayaan Republik Indonesia Nomor 50 Tahun 2015. Ejaan ini menggantikan Ejaan yang Disempurnakan.

## Brief history of Indonesia language spelling system

It's been a long time for Malay language to have their own spelling system. The spelling system that used in Dutch collonial era was Van Ophuijsen Spelling System. This spelling system was choosen simply because the Dutch need a spelling system that easy to use by the Dutch, and then they use latin script, reflecting contemporaneous Dutch phonology, and orthography. The spelling system was used from 1901

After the Indonesia gain independence, the Indonesian decided to make a new spelling system to replace Van Ophuijsen Spelling System. They named it Republican Spelling System or Soewandi Spelling System (named after the Indonesian Minister of Education at that time, Soewandi). It was used from 1947.

After more than 20 years Republican Spelling System used, they replace it again with Enhanced Indonesian Spelling System. It was officially used by Indonesia under Presidential Decree Number 57 1972.

And for now, the Indonesian are using Indonesian Spelling System that officially used since 2015 based on Indonesian Minister of Education and Culture Number 50 2015 regulation to replace Enhanced Indonesian Spelling System.

# License

MIT License (LICENSE-MIT or http://opensource.org/licenses/MIT)